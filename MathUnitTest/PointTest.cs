﻿using System;
using MathLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathUnitTest
{
    [TestClass]
    public class PointTest
    {
        [TestMethod]
        public void PointSubtraction()
        {
            Point point1 = new Point(4, 3);
            Point point2 = new Point(2, 2);
            Point point3 = point1 - point2;

            Assert.AreEqual(point3.X, 2);
        }
    }
}
