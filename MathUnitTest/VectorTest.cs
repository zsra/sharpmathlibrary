﻿using System;
using MathLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MathUnitTest
{
    [TestClass]
    public class VectorTest
    {
        [TestMethod]
        public void VectorAdding()
        {
            Vector vector1 = new Vector(10);
            Vector vector2 = new Vector(10);

            for (int i = 0; i < vector1.Length; i++)
            {
                vector1[i] = i + 1;
                vector2[i] = i + 1;
            }

            int expected = 0;

            for(int i = 0; i < 10; i++ )
            {
                expected += (i + 1);
            }

            expected *= 2;

            Assert.AreEqual(expected, (vector1 + vector2).Sum);
        }
    }
}
